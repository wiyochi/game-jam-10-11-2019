#ifndef CHANCE_H_
#define CHANCE_H_

#include "Case.hpp"

class Chance : public Case
{
public:
    Chance();
    Chance(sf::Vector2f const & pos, sf::Vector2f const & size);
    ~Chance();
    
private:
    
};

#endif